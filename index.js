var mysql = require("mysql");
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
  
var bodyParser = require('body-parser');

app.use(bodyParser());
app.listen(port);

console.log("Listening on Port: ", port);

//Database connection
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'password',
    database : 'testDB'
});

// Sample Query to Set DB Status
connection.query('SELECT ORDER_NUMBER FROM orders limit 1', (req, res) => {
    console.log("Sending sample query to set the server state.");
});

app.get('/status', (err, res) => {
	res.status(200);
	res.json({ mysqlConnection: connection.state });
	res.end();
});

app.post('/query', (req, res) => {
    console.log(`Incoming Request:`);
    console.log(`  Req Host: ${req.host}`);
    console.log(`  Req Headers: ${JSON.stringify(req.headers, null, 4)}`);
    console.log(`  Req Body: ${JSON.stringify(req.body, null, 4)}`);
    if (!req.body || !req.body.query) {
        console.log('Invalid Request, missing request body');
        res.status(400);
        res.json({ error: "Missing Request Body" });
        res.end();
    } else {
        console.log(`  Performing Query: '${req.body.query}'`);
        connection.query(req.body.query, function (error, results, fields) {
            if (error) throw error;
            console.log(`  Number of Records Found: ${results.length}`);
            res.json(results);
            res.end();
        });
    }
});