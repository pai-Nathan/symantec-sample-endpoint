# Symantec Mock REST Service

This is designed as a simple REST service to emmulate an endpoint created/managed by Symantec. This REST service allow for two different types of queries:

## GET /status
- Displays information about the status of mySQL connection

## POST /query
- Body Format: { "query":"<QUERY>"}
    - Ex. SELECT * FROM orders
- Response: JSON object of rows